import unittest

URLS = (
        'http://monsterfeet.com/grue.rss', # media enabled
        'https://fuse.pl/beton/atom.xml',
)

class FakeFilesTest(unittest.TestCase):

    def test_modes_file(self):

        import os
        import stat

        f = os.stat(__file__)

        self.assertTrue(
                stat.S_IFMT(f.st_mode) == stat.S_IFREG
        )

        self.assertTrue(
                stat.S_IMODE(f.st_mode) == 0o644
        )

    def test_modes_dir(self):

        import os
        import stat

        f = os.stat(os.path.dirname(os.path.realpath(__file__)))

        self.assertTrue(
                stat.S_IFMT(f.st_mode) == stat.S_IFDIR
        )

        self.assertTrue(
                stat.S_IMODE(f.st_mode) == 0o755
        )


class FeedHelperTest(unittest.TestCase):

    def test_initialization(self):

        import feed

        for url in URLS:
            self.assertTrue(feed.FeedHelper(url))

    def test_has_title(self):
        import feed
        import types

        for url in URLS:
            f = feed.FeedHelper(url)
            self.assertTrue(f)

            self.assertTrue(
                isinstance(f.entries_titles, types.GeneratorType)
            )

            self.assertTrue(
                isinstance(f.entries, list)
            )

            self.assertTrue(
                f.title != ""
            )


if __name__ == '__main__':
    unittest.main()
