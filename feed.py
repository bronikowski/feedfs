import requests
import feedparser

class EmptyFeedException(Exception):
    pass

class FeedHelper(object):

    def __init__(self, url):
        self.feed = None

        self.switch_url(url)

    def switch_url(self, url):
        self.feed = feedparser.parse(url)
        if not self.feed['entries']:
            raise EmptyFeedException

    @property
    def title(self):
        return self.feed['feed']['title']
    
    @property
    def entries(self):
        return self.feed['entries']
    
    @property
    def entries_titles(self):
        return self._reduce_entries('title')

    def _reduce_entries(self, key):
        return (f[key] for f in self.feed['entries'])


