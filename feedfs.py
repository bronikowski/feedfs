from fuse import FUSE, FuseOSError, Operations, LoggingMixIn
from sys import argv, exit
import os
import feed

class FeedFS(Operations, LoggingMixIn):

    def __init__(self):
        self.feed = feed.FeedHelper('http://monsterfeet.com/grue.rss')

    def readdir(self, path, fh):
        return self.feed.entries_titles

    def getattr(self, path, fh=None):
        print(path)
        print(path.split('/'))
        if len(path.split('/')) < 3:
            st = os.lstat('.')
        else:
            st = os.lstat('feedfs.py')
        print(st)
        return dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                                        'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))

if __name__ == '__main__':
    if len(argv) != 3:
        print('usage: %s <root> <mountpoint>' % argv[0])
        exit(1)

    import logging
    logging.basicConfig(level=logging.DEBUG)

    fuse = FUSE(FeedFS(), argv[2], foreground=True)
